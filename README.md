# pokemon-service

## Descripciòn: Este microservicio realiza el consumo de los servicios https://pokeapi.co/. usando spring-boot los servicios consumidos se realizan con @FeignClient y se realiza un registro en la base de datos usando MYSQL.
## en el proyecto se encuentra el script para la creacion de la base de datos y la tabla usada.

## Nota:Una vez que se ambiente el proyecto se deben de cambiar los datos de conexion a la base de datos.


## Metodos 

 - GET
 
 ## Host:
http://localhost:8080/pokemon

## Nota: Se usa localhost con el puerto 8080 pero si se realiza un despliegue en algun espacio se debe de usar el que corresponda. 
 
 ## parameter:
 #### Solo se debe de mandar un parametro por peticiòn
 
- path: pokemon/ditto
- path: pokemon-species/aegislash
- path: type/3        
- path: ability/battle-armor
 
 
 ## Headers.
 
 ### ip: 1.1.1.1.1.1
 
 ## Nota: se usa este parametro en todas las peticiones para que se guerde en bitacora.


 ## Ejemplo de peticiòn:
 
- URL: http://localhost:8080/pokemon?path=type/3
- METHOD: GET
- HEADER: ip:1.1.1.1.1.1