package mx.smart.rey.pokemon.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks; 
import org.mockito.MockitoAnnotations; 
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders; 
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
    
public class PokemonControllerTest {

	private MockMvc mockmvc;

	@InjectMocks
	private PokemonController ordersController;
 
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockmvc = MockMvcBuilders.standaloneSetup(ordersController).setControllerAdvice(new Exception())
				.setControllerAdvice().build();
	} 
	  
	
	@Test(expected = Test.None.class /* no exception expected */)
	public void pokemonTest() throws Exception { 
		this.mockmvc.perform(MockMvcRequestBuilders.get("/pokemon"))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andDo(MockMvcResultHandlers.print());
	}
}