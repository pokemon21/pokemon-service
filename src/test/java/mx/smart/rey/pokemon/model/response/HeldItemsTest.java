package mx.smart.rey.pokemon.model.response;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class HeldItemsTest {

	@InjectMocks
	private HeldItems data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getItemTest() throws Exception {
		data.getItem();
	}

	@Test(expected = Test.None.class)
	public void setItemTest() throws Exception {
		Item item = new Item();
		data.setItem(item);
	}

	@Test(expected = Test.None.class)
	public void getVersion_detailsTest() throws Exception {
		data.getVersion_details();
	}
	@Test(expected = Test.None.class)
	public void setVersion_detailsTest() throws Exception {
		ArrayList<VersionDetails> version_details = new ArrayList<>();
		data.setVersion_details(version_details);
	}  
}
