package mx.smart.rey.pokemon.model.response;
 

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GenerationITest {
	
	@InjectMocks
	private GenerationI data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getRed_blueTest() throws Exception {
		data.getRed_blue();
	}
	
	@Test(expected = Test.None.class)
	public void setRed_blueTest() throws Exception {
		RedBlue red_blue= new RedBlue();
		data.setRed_blue(red_blue);
	}
	
	@Test(expected = Test.None.class)
	public void getYellowTest() throws Exception {
		data.getYellow();
	}
	 

	@Test(expected = Test.None.class)
	public void setYellowTest() throws Exception {
		Yellow yellow = new Yellow();
		data.setYellow(yellow);
	}  
}
