package mx.smart.rey.pokemon.model.response;
 

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PokedexNumbersAegislashTest { 
	
	@InjectMocks
	private PokedexNumbersAegislash data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getEntry_numberTest() throws Exception {
		data.getEntry_number();
	}
	
	@Test(expected = Test.None.class)
	public void setEntry_numberTest() throws Exception {
		data.setEntry_number(1);
	}
	
	
	@Test(expected = Test.None.class)
	public void getPokedexTest() throws Exception {
		data.getPokedex();
	}
 
	@Test(expected = Test.None.class)
	public void setPokedexTest() throws Exception {
		PokedexAegislash pokedex = new PokedexAegislash();
		data.setPokedex(pokedex);
	} 
}
