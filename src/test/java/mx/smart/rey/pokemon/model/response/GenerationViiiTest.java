package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GenerationViiiTest {
	
	@InjectMocks
	private GenerationViii data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getIconsTest() throws Exception {
		data.getIcons();
	}

	@Test(expected = Test.None.class)
	public void setIconsTest() throws Exception {
		Icons icons = new Icons();
		data.setIcons(icons);
	}  
}
