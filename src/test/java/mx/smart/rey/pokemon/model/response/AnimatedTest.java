package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AnimatedTest  {
	
	@InjectMocks
	private Animated abilities;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getBack_defaultTest() throws Exception { 
		abilities.getBack_default();
	}
	
	@Test(expected = Test.None.class)
	public void setBack_defaultTest() throws Exception { 
		abilities.setBack_default("");
	}
	@Test(expected = Test.None.class)
	public void getBack_femaleTest() throws Exception { 
		abilities.getBack_female();
	}
	@Test(expected = Test.None.class)
	public void setBack_femaleTest() throws Exception { 
		abilities.setBack_female("");
	}
	@Test(expected = Test.None.class)
	public void getBack_shinyTest() throws Exception { 
		abilities.getBack_shiny();
	}
	@Test(expected = Test.None.class)
	public void setBack_shinyTest() throws Exception { 
		abilities.setBack_shiny("");
	}
 
	@Test(expected = Test.None.class)
	public void getBack_shiny_femaleTest() throws Exception { 
		abilities.getBack_shiny_female();
	}
	@Test(expected = Test.None.class)
	public void setBack_shiny_femaleTest() throws Exception { 
		abilities.setBack_shiny_female("");
	}
	
	@Test(expected = Test.None.class)
	public void getFront_defaultTest() throws Exception { 
		abilities.getFront_default();
	}
	
	@Test(expected = Test.None.class)
	public void setFront_defaultTest() throws Exception { 
		abilities.setFront_default("");
	} 
	@Test(expected = Test.None.class)
	public void getFront_femaleTest() throws Exception { 
		abilities.getFront_female();
	}
	@Test(expected = Test.None.class)
	public void setFront_femaleTest() throws Exception { 
		abilities.setFront_female("");
	} 
 
	@Test(expected = Test.None.class)
	public void getFront_shinyTest() throws Exception { 
		abilities.getFront_shiny();
	}
	
	@Test(expected = Test.None.class)
	public void setFront_shinyTest() throws Exception { 
		abilities.setFront_shiny("");
	} 
 
	@Test(expected = Test.None.class)
	public void getFront_shiny_femaleTest() throws Exception { 
		abilities.getFront_shiny_female();
	}

	@Test(expected = Test.None.class)
	public void setFront_shiny_femaleTest() throws Exception { 
		abilities.setFront_shiny_female("");
	}  
}
