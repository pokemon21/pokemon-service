package mx.smart.rey.pokemon.model.response;
 
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class RedBlueTest {
	
	@InjectMocks
	private RedBlue data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getBack_defaultTest() throws Exception {
		data.getBack_default();
	}
	
	@Test(expected = Test.None.class)
	public void setBack_defaultTest() throws Exception {
		data.setBack_default("");
	}
	
	@Test(expected = Test.None.class)
	public void getBack_grayTest() throws Exception {
		data.getBack_gray();
	}
	@Test(expected = Test.None.class)
	public void setBack_grayTest() throws Exception {
		data.setBack_gray("");
	}
	 
	@Test(expected = Test.None.class)
	public void getBack_transparentTest() throws Exception {
		data.getBack_transparent();
	}
	 
	@Test(expected = Test.None.class)
	public void setBack_transparentTest() throws Exception {
		data.setBack_transparent("");
	}
	 
	@Test(expected = Test.None.class)
	public void getFront_defaultTest() throws Exception {
		data.getFront_default();
	}
	@Test(expected = Test.None.class)
	public void setFront_defaultTest() throws Exception {
		data.setFront_default("");
	}
	
	@Test(expected = Test.None.class)
	public void getFront_grayTest() throws Exception {
		data.getFront_gray();
	}

	@Test(expected = Test.None.class)
	public void setFront_grayTest() throws Exception {
		data.setFront_gray("");
	}
	
	@Test(expected = Test.None.class)
	public void getFront_transparentTest() throws Exception {
		data.getFront_transparent();
	}
	@Test(expected = Test.None.class)
	public void setFront_transparentTest() throws Exception {
		data.setFront_transparent("");
	} 
}
