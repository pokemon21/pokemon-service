package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GenerationIiTest {
	
	@InjectMocks
	private GenerationIi data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getCrystalTest() throws Exception {
		data.getCrystal();
	}
	
	@Test(expected = Test.None.class)
	public void setCrystalTest() throws Exception {
		Crystal crystal= new Crystal();
		data.setCrystal(crystal);
	}
	@Test(expected = Test.None.class)
	public void getGoldTest() throws Exception {
		data.getGold();
	}
	 
	@Test(expected = Test.None.class)
	public void setGoldTest() throws Exception {
		Gold gold = new Gold();
		data.setGold( gold);
	}

	@Test(expected = Test.None.class)
	public void getSilverTest() throws Exception {
		data.getSilver();
	}
	 

	@Test(expected = Test.None.class)
	public void setSilverTest() throws Exception {
		Silver silver = new Silver();
		data.setSilver( silver );
	} 
}
