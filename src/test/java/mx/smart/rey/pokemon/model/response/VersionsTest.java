package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class VersionsTest {
	
	
	@InjectMocks
	private Versions data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getGeneration_iTest() throws Exception {
		data.getGeneration_i();
	}
	
	@Test(expected = Test.None.class)
	public void setGeneration_i() throws Exception {
		GenerationI generation_i= new GenerationI();
		data.setGeneration_i(generation_i);
	}
	@Test(expected = Test.None.class)
	public void getGeneration_iiTest() throws Exception {
		data.getGeneration_ii();
	}
	@Test(expected = Test.None.class)
	public void setGeneration_iiTest() throws Exception {
		GenerationIi generation_i= new GenerationIi();
		data.setGeneration_ii(generation_i);
	}
	 
	@Test(expected = Test.None.class)
	public void getGeneration_iiiTest() throws Exception {
		data.getGeneration_iii();
	}
 
	@Test(expected = Test.None.class)
	public void setGeneration_iiiTest() throws Exception {
		GenerationIii generation_i= new GenerationIii();
		data.setGeneration_iii(generation_i);
	}
	 
	@Test(expected = Test.None.class)
	public void getGeneration_ivTest() throws Exception {
		data.getGeneration_iv();
	}
	 
	@Test(expected = Test.None.class)
	public void setGeneration_ivTest() throws Exception {
		GenerationIv generation_i= new GenerationIv();
		data.setGeneration_iv(generation_i);
	}
	 
	@Test(expected = Test.None.class)
	public void getGeneration_vTest() throws Exception {
		data.getGeneration_v();
	}
	@Test(expected = Test.None.class)
	public void setGeneration_vTest() throws Exception {
		GenerationV generation_i= new GenerationV();
		data.setGeneration_v(generation_i);
	} 
	
	
  
	@Test(expected = Test.None.class)
	public void getGeneration_viTest() throws Exception {
		data.getGeneration_vi();
	}
	@Test(expected = Test.None.class)
	public void setGeneration_viTest() throws Exception {
		Generationvi generation_i= new Generationvi();
		data.setGeneration_vi(generation_i);
	}


	@Test(expected = Test.None.class)
	public void getGeneration_viiTest() throws Exception {
		data.getGeneration_vii();
	}
 
	@Test(expected = Test.None.class)
	public void setGeneration_viiTest() throws Exception {
		GenerationVii generation_i= new GenerationVii();
		data.setGeneration_vii(generation_i);
	}
	 
	@Test(expected = Test.None.class)
	public void getGeneration_viiiTest() throws Exception {
		data.getGeneration_viii();
	}
	 
	@Test(expected = Test.None.class)
	public void setGeneration_viiiTest() throws Exception {
		GenerationViii generation_i= new GenerationViii();
		data.setGeneration_viii(generation_i);
	}
	  
}
