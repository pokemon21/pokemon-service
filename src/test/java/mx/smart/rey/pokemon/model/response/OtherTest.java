package mx.smart.rey.pokemon.model.response;
 

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class OtherTest {
	
	@InjectMocks
	private Other data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getDream_worldTest() throws Exception {
		data.getDream_world();
	}
	
	@Test(expected = Test.None.class)
	public void setDream_worldTest() throws Exception {
		DreamWorld dream_world = new DreamWorld();
		data.setDream_world(dream_world);
	}
	
	@Test(expected = Test.None.class)
	public void getHomeTest() throws Exception {
		data.getHome();
	}
	

	@Test(expected = Test.None.class)
	public void setHomeTest() throws Exception {
		Home home = new Home();
		data.setHome(home);
	}
  
 
	@Test(expected = Test.None.class)
	public void getOfficial_artworkTest() throws Exception {
		data.getOfficial_artwork();
	}
	
	@Test(expected = Test.None.class)
	public void setOfficial_artworkTest() throws Exception {
		OfficialArtwork official_artwork = new OfficialArtwork();
		data.setOfficial_artwork(official_artwork);
	}
	  
}
