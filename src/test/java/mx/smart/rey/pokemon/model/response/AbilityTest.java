package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AbilityTest{
 
	@InjectMocks
	private Ability abilities;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getNameTest() throws Exception { 
		abilities.getName();
	}
    
	@Test(expected = Test.None.class)
	public void setNameTest() throws Exception { 
		abilities.setName("");
	}
	@Test(expected = Test.None.class)
	public void getUrlTest() throws Exception { 
		abilities.getUrl();
	}
	 
	@Test(expected = Test.None.class)
	public void setUrlTest() throws Exception { 
		abilities.setUrl("");
	} 
}
