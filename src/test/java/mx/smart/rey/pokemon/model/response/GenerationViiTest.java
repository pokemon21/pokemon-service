package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GenerationViiTest {
	
	@InjectMocks
	private GenerationVii data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getIconsTest() throws Exception {
		data.getIcons();
	}
	
	@Test(expected = Test.None.class)
	public void setIconsTest() throws Exception {
		Icons icons= new Icons();
		data.setIcons(icons);
	}
	@Test(expected = Test.None.class)
	public void getUltra_sun_ultra_moonTest() throws Exception {
		data.getUltra_sun_ultra_moon();
	}
	@Test(expected = Test.None.class)
	public void setUltra_sun_ultra_moonTest() throws Exception {
		UltraSunUltraMoon ultra_sun_ultra_moon = new UltraSunUltraMoon();
		data.setUltra_sun_ultra_moon(ultra_sun_ultra_moon);
	} 
}
