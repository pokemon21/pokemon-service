package mx.smart.rey.pokemon.model.response;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class MovesTest {

	@InjectMocks
	private Moves data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getMoveTest() throws Exception {
		data.getMove();
	}
	
	@Test(expected = Test.None.class)
	public void setMoveTest() throws Exception {
		Move move = new Move();
		data.setMove(move);
	}

 
	@Test(expected = Test.None.class)
	public void getVersion_group_detailsTest() throws Exception {
		data.getVersion_group_details();
	}
	 
	@Test(expected = Test.None.class)
	public void setVersion_group_detailsTest() throws Exception {
		ArrayList<VersionGroupDetails> version_group_details = new ArrayList<>();
		data.setVersion_group_details(version_group_details);
	}
	 
	@Test(expected = Test.None.class)
	public void getNameTest() throws Exception {
		data.getName();
	}

	@Test(expected = Test.None.class)
	public void setNameTest() throws Exception { 
		data.setName("");
	}
	@Test(expected = Test.None.class)
	public void getUrlTest() throws Exception {
		data.getUrl();
	}

	@Test(expected = Test.None.class)
	public void setUrlTest() throws Exception { 
		data.setUrl("");
	}   
}
