package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class EffectEntriesArmorTest {
	

	@InjectMocks
	private EffectEntriesArmor data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getEffectTest() throws Exception {
		data.getEffect();
	}
	
	
	@Test(expected = Test.None.class)
	public void setEffectTest() throws Exception {
		data.setEffect("");
	}
	@Test(expected = Test.None.class)
	public void getLanguageTest() throws Exception {
		data.getLanguage();
	}
	
	@Test(expected = Test.None.class)
	public void setLanguageTest() throws Exception {
		LanguageType language= new LanguageType();
		data.setLanguage(language);
	}
	 
	@Test(expected = Test.None.class)
	public void getShort_effectTest() throws Exception {
		data.getShort_effect();
	}

	@Test(expected = Test.None.class)
	public void setShort_effectTest() throws Exception { 
		data.setShort_effect("");
	}
	  
}
