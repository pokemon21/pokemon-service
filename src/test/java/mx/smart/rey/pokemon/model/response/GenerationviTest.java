package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GenerationviTest {

	@InjectMocks
	private Generationvi data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getOmegaruby_alphasapphireTest() throws Exception {
		data.getOmegaruby_alphasapphire();
	}
	
	@Test(expected = Test.None.class)
	public void setOmegaruby_alphasapphireTest() throws Exception {
		OmegarubyAlphasapphire omegaruby_alphasapphire= new OmegarubyAlphasapphire();
		data.setOmegaruby_alphasapphire(omegaruby_alphasapphire);
	}
	@Test(expected = Test.None.class)
	public void getX_yTest() throws Exception {
		data.getX_y();
	}
	
	@Test(expected = Test.None.class)
	public void setX_yTest() throws Exception {
		Xy x_y = new Xy();
		data.setX_y(x_y);
	} 
}
