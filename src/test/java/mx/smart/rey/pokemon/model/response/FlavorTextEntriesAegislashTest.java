package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class FlavorTextEntriesAegislashTest {
	
	@InjectMocks
	private FlavorTextEntriesAegislash data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getFlavor_textTest() throws Exception {
		data.getFlavor_text();
	}
	
	
	@Test(expected = Test.None.class)
	public void setFlavor_text() throws Exception {
		data.setFlavor_text("");
	}
	@Test(expected = Test.None.class)
	public void getLanguageTest() throws Exception {
		data.getLanguage();
	}
	@Test(expected = Test.None.class)
	public void setLanguageTest() throws Exception {
		LanguageType language = new LanguageType();
		data.setLanguage(language);
	}
	@Test(expected = Test.None.class)
	public void getVersionTest() throws Exception {
		data.getVersion();
	}
 
	@Test(expected = Test.None.class)
	public void setVersionTest() throws Exception {
		Version version = new Version();
		data.setVersion(version);
	}  
}
