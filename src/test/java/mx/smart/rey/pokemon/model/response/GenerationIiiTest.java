package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GenerationIiiTest {
	
	@InjectMocks
	private GenerationIii data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getEmeraldTest() throws Exception {
		data.getEmerald();
	}
	
	@Test(expected = Test.None.class)
	public void setEmeraldTest() throws Exception {
		Emerald emerald= new Emerald();
		data.setEmerald(emerald);
	}
	 

	@Test(expected = Test.None.class)
	public void getFirered_leafgreen() throws Exception {
		data.getFirered_leafgreen();
	}
	@Test(expected = Test.None.class)
	public void setFirered_leafgreenTest() throws Exception {
		FireredLeafgreen firered_leafgreen = new FireredLeafgreen();
		data.setFirered_leafgreen(firered_leafgreen);
	}
	 
	@Test(expected = Test.None.class)
	public void getRuby_sapphireTest() throws Exception {
		data.getRuby_sapphire();
	}
	 
	
	@Test(expected = Test.None.class)
	public void setRuby_sapphireTest() throws Exception {
		RubySapphire ruby_sapphire = new RubySapphire();
		data.setRuby_sapphire(ruby_sapphire);
	} 
}
