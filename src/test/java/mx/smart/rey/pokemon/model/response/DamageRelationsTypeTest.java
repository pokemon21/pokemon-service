package mx.smart.rey.pokemon.model.response;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class DamageRelationsTypeTest {
	
	@InjectMocks
	private DamageRelationsType data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getDouble_damage_fromTest() throws Exception {
		data.getDouble_damage_from();
	}
	
	@Test(expected = Test.None.class)
	public void setDouble_damage_fromTest() throws Exception {
		ArrayList<DoubleDamageFromType> double_damage_from = new ArrayList<>();
		data.setDouble_damage_from(double_damage_from);
	}
 
	@Test(expected = Test.None.class)
	public void getDouble_damage_toTest() throws Exception {
		data.getDouble_damage_to();
	}

	@Test(expected = Test.None.class)
	public void setDouble_damage_toTest() throws Exception {
		ArrayList<DoubleDamageToType> doubleDamageToType = new ArrayList<>();
		data.setDouble_damage_to(doubleDamageToType);
	} 
	
	
	@Test(expected = Test.None.class)
	public void getHalf_damage_fromTest() throws Exception {
		data.getHalf_damage_from();
	}

	@Test(expected = Test.None.class)
	public void setHalf_damage_fromTest() throws Exception {
		ArrayList<DalfDamageFromType> doubleDamageToType = new ArrayList<>();
		data.setHalf_damage_from(doubleDamageToType);
	} 

	@Test(expected = Test.None.class)
	public void getHalf_damage_toTest() throws Exception {
		data.getHalf_damage_to();
	}

	@Test(expected = Test.None.class)
	public void setHalf_damage_to() throws Exception {
		ArrayList<HalfDamageToType> doubleDamageToType = new ArrayList<>();
		data.setHalf_damage_to(doubleDamageToType);
	} 
	@Test(expected = Test.None.class)
	public void getNo_damage_fromTest() throws Exception {
		data.getNo_damage_from();
	}

	@Test(expected = Test.None.class)
	public void setNo_damage_fromTest() throws Exception {
		ArrayList<NoDamageFromType> doubleDamageToType = new ArrayList<>();
		data.setNo_damage_from(doubleDamageToType);
	} 
 
}
