package mx.smart.rey.pokemon.model.response;
 

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PokemonListTypeTest {
	
	@InjectMocks
	private PokemonListType data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getPokemonTest() throws Exception {
		data.getPokemon();
	}
	
	@Test(expected = Test.None.class)
	public void setPokemonTest() throws Exception {
		PokemonType pokemon = new PokemonType();
		data.setPokemon(pokemon);
	}
	
	@Test(expected = Test.None.class)
	public void getSlotTest() throws Exception {
		data.getSlot();
	} 
	@Test(expected = Test.None.class)
	public void setSlotTest() throws Exception { 
		data.setSlot("");
	} 
}
