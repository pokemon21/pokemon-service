package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class VersionGroupDetailsTest {

	@InjectMocks
	private VersionGroupDetails data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getGroupTest() throws Exception {
		data.getGroup();
	}
	
	@Test(expected = Test.None.class)
	public void setGroupTest() throws Exception {
		VersionGroup group = new VersionGroup();
		data.setGroup(group);
	}
	
	@Test(expected = Test.None.class)
	public void getLevel_learned_atTest() throws Exception {
		data.getLevel_learned_at();
	}
  
	@Test(expected = Test.None.class)
	public void setLevel_learned_atTest() throws Exception { 
		data.setLevel_learned_at(1);
	}


	@Test(expected = Test.None.class)
	public void getMove_learn_methodTest() throws Exception {
		data.getMove_learn_method();
	}
	
	@Test(expected = Test.None.class)
	public void setMove_learn_methodTest() throws Exception {
		MoveIearnMethod move_learn_method = new MoveIearnMethod();
		data.setMove_learn_method(move_learn_method);
	}  
}
