package mx.smart.rey.pokemon.model.response;
 

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GeneraAegislashTest {


	@InjectMocks
	private GeneraAegislash data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getGenusTest() throws Exception {
		data.getGenus();
	}
	
	@Test(expected = Test.None.class)
	public void setGenusTest() throws Exception {
		data.setGenus("");
	}
	@Test(expected = Test.None.class)
	public void getLanguageTest() throws Exception {
		data.getLanguage();
	}
	 
	@Test(expected = Test.None.class)
	public void setLanguageTest() throws Exception {
		LanguageType language= new LanguageType();
		data.setLanguage(language);
	}  
}
