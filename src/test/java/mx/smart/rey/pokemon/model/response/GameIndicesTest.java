package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GameIndicesTest {

	@InjectMocks
	private GameIndices data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getGame_indexTest() throws Exception {
		data.getGame_index();
	}

	 
	@Test(expected = Test.None.class)
	public void setGame_indexTest() throws Exception {
		data.setGame_index(1);
	}
	 
	@Test(expected = Test.None.class)
	public void getVersionTest() throws Exception {
		data.getVersion();
	}

	@Test(expected = Test.None.class)
	public void setVersionTest() throws Exception {
		Version version= new Version();
		data.setVersion(version);
	} 
}
