package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class FormsTest{

	@InjectMocks
	private Forms data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getNameTest() throws Exception {
		data.getName();
	}
	
	@Test(expected = Test.None.class)
	public void setNameTest() throws Exception {
		data.setName("");
	}
	
	@Test(expected = Test.None.class)
	public void getUrlTest() throws Exception {
		data.getUrl();
	}

	@Test(expected = Test.None.class)
	public void setUrlTest() throws Exception {
		data.setUrl("");
	}
}
