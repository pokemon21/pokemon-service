package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GenerationVTest {
	
	@InjectMocks
	private GenerationV data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getBlack_whiteTest() throws Exception {
		data.getBlack_white();
	}
	

	@Test(expected = Test.None.class)
	public void setBlack_whiteTest() throws Exception {
		BlackWhite black_white= new BlackWhite();
		data.setBlack_white(black_white);
	}  	
}
