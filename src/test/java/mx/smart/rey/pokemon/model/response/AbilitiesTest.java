package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AbilitiesTest {

	@InjectMocks
 private Abilities abilities;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	
	
	@Test(expected = Test.None.class)
	public void getAbility() throws Exception { 
		abilities.getAbility();
	}
	
	@Test(expected = Test.None.class)
	public void setAbilityTest() throws Exception {
		Ability ability = new Ability();
		abilities.setAbility(ability);
	}
	@Test(expected = Test.None.class)
	public void getSlotTest() throws Exception { 
		abilities.getSlot();
	} 
	@Test(expected = Test.None.class)
	public void setSlotTest() throws Exception { 
		abilities.setSlot(0);
	}
	@Test(expected = Test.None.class)
	public void getIs_hiddenTest() throws Exception { 
		abilities.getIs_hidden();
	} 
	@Test(expected = Test.None.class)
	public void setIs_hiddenTest() throws Exception { 
		abilities.setIs_hidden("");
	}
	 

}
