package mx.smart.rey.pokemon.model.response;
 
import java.util.ArrayList;
 
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class DataResponseTest {

	@InjectMocks
	private DataResponse data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getAbilitiesTest() throws Exception {
		data.getAbilities();
	}
	
	 
	@Test(expected = Test.None.class)
	public void setAbilitiesTest() throws Exception {
		ArrayList<Abilities> abilities = new ArrayList<>();
		data.setAbilities(abilities);
	}
	
	@Test(expected = Test.None.class)
	public void getFormsTest() throws Exception {
		data.getForms();
	}
	@Test(expected = Test.None.class)
	public void setFormsTest() throws Exception {
		ArrayList<Forms> forms = new ArrayList<>();
		data.setForms(forms);
	}
 
	@Test(expected = Test.None.class)
	public void getHeightTest() throws Exception {
		data.getHeight();
	}

	@Test(expected = Test.None.class)
	public void setHeightTest() throws Exception { 
		data.setHeight(0);
	}
	
	@Test(expected = Test.None.class)
	public void getIdTest() throws Exception {
		data.getId();
	}  
	@Test(expected = Test.None.class)
	public void setIdTest() throws Exception { 
		data.setId(0);
	}
 
	@Test(expected = Test.None.class)
	public void getMovesTest() throws Exception {
		data.getMoves();
	}
 
	@Test(expected = Test.None.class)
	public void setMovesTest() throws Exception { 
		ArrayList<Moves> moves= new ArrayList<>();
		data.setMoves(moves);
	}

	@Test(expected = Test.None.class)
	public void getBase_experienceTest() throws Exception {
		data.getBase_experience();
	}
	
	@Test(expected = Test.None.class)
	public void setBase_experienceTest() throws Exception {  
		data.setBase_experience("");
	}
 

	@Test(expected = Test.None.class)
	public void getGame_indicesTest() throws Exception {
		data.getGame_indices();
	}

	
	@Test(expected = Test.None.class)
	public void setGame_indicesTest() throws Exception { 
		ArrayList<GameIndices> game_indices = new ArrayList<>();
		data.setGame_indices(game_indices);
	}

	@Test(expected = Test.None.class)
	public void getHeld_itemsTest() throws Exception {
		data.getHeld_items();
	}
 
	@Test(expected = Test.None.class)
	public void setHeld_itemsTest() throws Exception { 
		ArrayList<HeldItems> game_indices = new ArrayList<>();
		data.setHeld_items(game_indices);
	}
 
	@Test(expected = Test.None.class)
	public void getIs_defaultTest() throws Exception {
		data.getIs_default();
	}
	 

	@Test(expected = Test.None.class)
	public void setIs_defaultTest() throws Exception {  
		data.setIs_default(true);
	}

	@Test(expected = Test.None.class)
	public void getLocation_area_encountersTest() throws Exception {
		data.getLocation_area_encounters();
	}

	@Test(expected = Test.None.class)
	public void setLocation_area_encountersTest() throws Exception {  
		data.setLocation_area_encounters("");
	}
	@Test(expected = Test.None.class)
	public void getNameTest() throws Exception {
		data.getName();
	}

	@Test(expected = Test.None.class)
	public void setNameTest() throws Exception {  
		data.setName("");
	}

	@Test(expected = Test.None.class)
	public void getOrderTest() throws Exception {
		data.getOrder();
	}

	@Test(expected = Test.None.class)
	public void setOrderTest() throws Exception {  
		data.setOrder(0);
	}

	@Test(expected = Test.None.class)
	public void getPast_typesTest() throws Exception {
		data.getPast_types();
	}

	@Test(expected = Test.None.class)
	public void setPast_typesTest() throws Exception {
		ArrayList<String> past_types = new ArrayList<>();
		data.setPast_types(past_types);
	}
	@Test(expected = Test.None.class)
	public void getSpeciesTest() throws Exception {
		data.getSpecies();
	}
 
	@Test(expected = Test.None.class)
	public void setSpeciesTest() throws Exception {
		Species species = new Species();
		data.setSpecies(species);
	}
 
	@Test(expected = Test.None.class)
	public void getSpritesTest() throws Exception {
		data.getSprites();
	}

	@Test(expected = Test.None.class)
	public void setSpritesTest() throws Exception {
		Sprites species = new Sprites();
		data.setSprites(species);
	}
	
	@Test(expected = Test.None.class)
	public void getStatsTest() throws Exception {
		data.getStats();
	}
	
	@Test(expected = Test.None.class)
	public void setStatsTest() throws Exception {
		ArrayList<Stats> stats = new ArrayList<>();
		data.setStats(stats);
	}
	
	@Test(expected = Test.None.class)
	public void getDamage_relationsTest() throws Exception {
		data.getDamage_relations();
	}
  
	@Test(expected = Test.None.class)
	public void setDamage_relationsTest() throws Exception {
		DamageRelationsType damage_relations = new DamageRelationsType();
		data.setDamage_relations(damage_relations);
	}

	@Test(expected = Test.None.class)
	public void getGenerationTest() throws Exception {
		data.getGeneration();
	}

	@Test(expected = Test.None.class)
	public void setGenerationTest() throws Exception {
		GenerationType generation = new GenerationType();
		data.setGeneration(generation);
	}

	@Test(expected = Test.None.class)
	public void getNamesTest() throws Exception {
		data.getNames();
	}

	@Test(expected = Test.None.class)
	public void setNamesTest() throws Exception {
		ArrayList<NamesType> names = new ArrayList<>();
		data.setNames(names);
	}
	@Test(expected = Test.None.class)
	public void getPokemonTest() throws Exception {
		data.getPokemon();
	}
 
	@Test(expected = Test.None.class)
	public void setPokemonTest() throws Exception {
		ArrayList<PokemonListType> pokemon = new ArrayList<>();
		data.setPokemon(pokemon);
	}
	 
	@Test(expected = Test.None.class)
	public void getBase_happinessTest() throws Exception {
		data.getBase_happiness();
	}
 
	@Test(expected = Test.None.class)
	public void setBase_happinessTest() throws Exception { 
		data.setBase_happiness("");
	}
 
	@Test(expected = Test.None.class)
	public void getCapture_rateTest() throws Exception {
		data.getCapture_rate();
	}
	@Test(expected = Test.None.class)
	public void setCapture_rateTest() throws Exception { 
		data.setBase_happiness("");
	}
 
	@Test(expected = Test.None.class)
	public void getColorTest() throws Exception {
		data.getColor();
	}
 
	
	@Test(expected = Test.None.class)
	public void setColorTest() throws Exception { 
		ColorAegislash color = new ColorAegislash();
		data.setColor(color);
	}

	@Test(expected = Test.None.class)
	public void getEgg_groupsTest() throws Exception {
		data.getEgg_groups();
	}

	@Test(expected = Test.None.class)
	public void setEgg_groupsTest() throws Exception { 
		ArrayList<eggGroupsAegislash> egg_groups = new ArrayList<>();
		data.setEgg_groups(egg_groups);
	}

	@Test(expected = Test.None.class)
	public void getEvolution_chainTest() throws Exception {
		data.getEvolution_chain();
	}

	@Test(expected = Test.None.class)
	public void setEvolution_chainTest() throws Exception { 
		EvolutionChainAegislash evolution_chain = new EvolutionChainAegislash();
		data.setEvolution_chain(evolution_chain);
	}

	@Test(expected = Test.None.class)
	public void getEvolves_from_speciesTest() throws Exception {
		data.getEvolves_from_species();
	}

	@Test(expected = Test.None.class)
	public void setEvolves_from_speciesTest() throws Exception { 
		EvolvesFromSpeciesAegislash evolves_from_species = new EvolvesFromSpeciesAegislash();
		data.setEvolves_from_species(evolves_from_species);
	}

	@Test(expected = Test.None.class)
	public void getFlavor_text_entriesTest() throws Exception {
		data.getFlavor_text_entries();
	}

	@Test(expected = Test.None.class)
	public void setFlavor_text_entriesTest() throws Exception { 
		ArrayList<FlavorTextEntriesAegislash> flavor_text_entries = new ArrayList<>();
		data.setFlavor_text_entries(flavor_text_entries);
	}
	@Test(expected = Test.None.class)
	public void getForm_descriptionsTest() throws Exception {
		data.getForm_descriptions();
	}
 
	@Test(expected = Test.None.class)
	public void setForm_descriptionsTest() throws Exception { 
		ArrayList<FormDescriptionsAegislash> form_descriptions = new ArrayList<>();
		data.setForm_descriptions(form_descriptions);
	}
	@Test(expected = Test.None.class)
	public void getForms_switchableTest() throws Exception {
		data.getForms_switchable();
	}
 
	@Test(expected = Test.None.class)
	public void setForms_switchableTest() throws Exception {  
		data.setForms_switchable(true);
	}
	 
	@Test(expected = Test.None.class)
	public void getGender_rateTest() throws Exception {
		data.getGender_rate();
	}
 
	@Test(expected = Test.None.class)
	public void setGender_rateTest() throws Exception {  
		data.setGender_rate(1);
	}
	 
	@Test(expected = Test.None.class)
	public void getGeneraTest() throws Exception {
		data.getGenera();
	}

	@Test(expected = Test.None.class)
	public void setGeneraTest() throws Exception {  
		ArrayList<GeneraAegislash> genera = new ArrayList<>();
		data.setGenera(genera);
	} 

	@Test(expected = Test.None.class)
	public void getGrowth_rateTest() throws Exception {
		data.getGrowth_rate();
	}

	@Test(expected = Test.None.class)
	public void setGrowth_rateTest() throws Exception {  
		GrowthRateAegislash growth_rate= new GrowthRateAegislash();
		data.setGrowth_rate(growth_rate);
	} 

	@Test(expected = Test.None.class)
	public void getHabitatTest() throws Exception {
		data.getHabitat();
	}

	@Test(expected = Test.None.class)
	public void setHabitatTest() throws Exception {   
		data.setHabitat("");
	} 

	@Test(expected = Test.None.class)
	public void getHas_gender_differencesTest() throws Exception {
		data.getHas_gender_differences();
	}
	@Test(expected = Test.None.class)
	public void setHas_gender_differencesTest() throws Exception {   
		data.setHas_gender_differences(false);
	}
 
	@Test(expected = Test.None.class)
	public void getHatch_counterTest() throws Exception {
		data.getHatch_counter();
	}
	 
	@Test(expected = Test.None.class)
	public void setHatch_counterTest() throws Exception {   
		data.setHatch_counter(1);
	}
	 
	@Test(expected = Test.None.class)
	public void getIs_babyTest() throws Exception {
		data.getIs_baby();
	}
	 

	@Test(expected = Test.None.class)
	public void setIs_babyTest() throws Exception {   
		data.setIs_baby(false);
	}

	@Test(expected = Test.None.class)
	public void getIs_legendaryTest() throws Exception {
		data.getIs_legendary();
	}
	@Test(expected = Test.None.class)
	public void setIs_legendaryTest() throws Exception {   
		data.setIs_legendary(false);
	}
	 
	@Test(expected = Test.None.class)
	public void getIs_mythicalTest() throws Exception {
		data.getIs_mythical();
	} 

	@Test(expected = Test.None.class)
	public void setIs_mythicalTest() throws Exception {   
		data.setIs_mythical(false);
	}

	@Test(expected = Test.None.class)
	public void getPokedex_numbersTest() throws Exception {
		data.getPokedex_numbers();
	}

	@Test(expected = Test.None.class)
	public void setPokedex_numbersTest() throws Exception { 
		ArrayList<PokedexNumbersAegislash> pokedex_numbers = new ArrayList<>();
		data.setPokedex_numbers(pokedex_numbers);
	}

	@Test(expected = Test.None.class)
	public void getShapeTest() throws Exception {
		data.getShape();
	}
	@Test(expected = Test.None.class)
	public void setShapeTest() throws Exception { 
		ShapeAegislash shape = new ShapeAegislash();
		data.setShape(shape);
	}

	@Test(expected = Test.None.class)
	public void getVarietiesTest() throws Exception {
		data.getVarieties();
	}

	@Test(expected = Test.None.class)
	public void setVarietiesTest() throws Exception { 
		ArrayList<VarietiesAegislash> varieties = new ArrayList<>();
		data.setVarieties(varieties);
	}
	@Test(expected = Test.None.class)
	public void getIs_main_seriesTest() throws Exception {
		data.getIs_main_series();
	}

	@Test(expected = Test.None.class)
	public void setIs_main_seriesTest() throws Exception {  
		data.setIs_main_series(false);
	}


	@Test(expected = Test.None.class)
	public void getEffect_entriesTest() throws Exception {
		data.getEffect_entries();
	}
	
	@Test(expected = Test.None.class)
	public void setEffect_entriesTest() throws Exception {  
		ArrayList<EffectEntriesArmor> effect_entries = new ArrayList<>();
		data.setEffect_entries(effect_entries);
	}

	@Test(expected = Test.None.class)
	public void getCountTest() throws Exception {
		data.getCount();
	}

	@Test(expected = Test.None.class)
	public void setCountTest() throws Exception {  
		ArrayList<EffectEntriesArmor> effect_entries = new ArrayList<>();
		data.setCount(1);
	}

	@Test(expected = Test.None.class)
	public void getNextTest() throws Exception {
		data.getNext();
	}

	@Test(expected = Test.None.class)
	public void setNextTest() throws Exception {   
		data.setNext("");
	}
	@Test(expected = Test.None.class)
	public void getPreviousTest() throws Exception {
		data.getPrevious();
	}

	@Test(expected = Test.None.class)
	public void setPreviousTest() throws Exception {   
		data.setPrevious("");
	}

	@Test(expected = Test.None.class)
	public void getResultsTest() throws Exception {
		data.getResults();
	}

	@Test(expected = Test.None.class)
	public void setResultsTest() throws Exception {   
		ArrayList<ResultsLimit> results= new ArrayList<>();
		data.setResults(results);
	}  
}
