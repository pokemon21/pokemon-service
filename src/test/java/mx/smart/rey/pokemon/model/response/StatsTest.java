package mx.smart.rey.pokemon.model.response;
 
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class StatsTest {
	
	@InjectMocks
	private Stats data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getBase_statTest() throws Exception {
		data.getBase_stat();
	}
	
	@Test(expected = Test.None.class)
	public void setBase_statTest() throws Exception {
		data.setBase_stat(1);
	}
	
	@Test(expected = Test.None.class)
	public void getEffortTest() throws Exception {
		data.getEffort();
	} 
	@Test(expected = Test.None.class)
	public void setEffortTest() throws Exception {
		data.setEffort(1);
	}
	 
	@Test(expected = Test.None.class)
	public void getStatTest() throws Exception {
		data.getStat();
	} 
	@Test(expected = Test.None.class)
	public void setStatTest() throws Exception {
		Stat stat = new Stat();
		data.setStat(stat);
	}  
}
