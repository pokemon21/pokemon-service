package mx.smart.rey.pokemon.model.response;
 

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class EmeraldTest {
	

	@InjectMocks
	private Emerald data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getFront_defaultTest() throws Exception {
		data.getFront_default();
	}
	
	@Test(expected = Test.None.class)
	public void setFront_defaultTest() throws Exception {
		data.setFront_default("");
	}

	@Test(expected = Test.None.class)
	public void getFront_shinyTest() throws Exception {
		data.getFront_shiny();
	}
	@Test(expected = Test.None.class)
	public void setFront_shinyTest() throws Exception {
		data.setFront_shiny("");
	}  
}
