package mx.smart.rey.pokemon.model.response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GenerationIvTest {
	
	@InjectMocks
	private GenerationIv data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getDiamond_pearlTest() throws Exception {
		data.getDiamond_pearl();
	}
	
	@Test(expected = Test.None.class)
	public void setDiamond_pearlTest() throws Exception {
		DiamondPearl diamond_pearl= new DiamondPearl();
		data.setDiamond_pearl(diamond_pearl);
	}
	@Test(expected = Test.None.class)
	public void getHeartgold_soulsilverTest() throws Exception {
		data.getHeartgold_soulsilver();
	}
	 
	@Test(expected = Test.None.class)
	public void setHeartgold_soulsilverTest() throws Exception {
		HeartgoldSoulsilver heartgold_soulsilver = new HeartgoldSoulsilver();
		data.setHeartgold_soulsilver(heartgold_soulsilver);
	}
	@Test(expected = Test.None.class)
	public void getPlatinumTest() throws Exception { 
		data.getPlatinum();
	}
	@Test(expected = Test.None.class)
	public void setPlatinumTest() throws Exception {
		Platinum platinum = new Platinum();
		data.setPlatinum(platinum);
	}
   
}
