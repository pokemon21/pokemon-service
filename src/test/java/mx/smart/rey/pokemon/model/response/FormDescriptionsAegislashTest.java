package mx.smart.rey.pokemon.model.response;
 

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class FormDescriptionsAegislashTest {
	
	@InjectMocks
	private FormDescriptionsAegislash data;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	} 
	@Test(expected = Test.None.class)
	public void getDescriptionTest() throws Exception {
		data.getDescription();
	}
	
	@Test(expected = Test.None.class)
	public void setDescriptionTest() throws Exception {
		data.setDescription("");
	}
	
	@Test(expected = Test.None.class)
	public void getLanguageTest() throws Exception {
		data.getLanguage();
	}
	 
	@Test(expected = Test.None.class)
	public void setLanguageTest() throws Exception {
		LanguageType language = new LanguageType();
		data.setLanguage(language);
	} 
}
