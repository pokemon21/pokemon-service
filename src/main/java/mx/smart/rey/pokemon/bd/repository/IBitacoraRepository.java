package mx.smart.rey.pokemon.bd.repository;

import org.springframework.data.jpa.repository.JpaRepository; 
import org.springframework.stereotype.Repository;

import mx.smart.rey.pokemon.domain.Bitacora; 
 
@Repository
public interface IBitacoraRepository extends JpaRepository<Bitacora, Integer> {
 
}