package mx.smart.rey.pokemon.util;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public final class PokemonConstantes { 
	 
	
	private PokemonConstantes() {
		throw new IllegalStateException("PokemonConstantes class");
	} // previene instanciacion

}