package mx.smart.rey.pokemon.util;

import java.sql.Timestamp;
import java.time.Instant;

/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public final class Utils {
	 
	
	/**
	 * Constructor privado.
	 */
	private Utils() {
		// Sin contenido
	}
	
	/**
	 * Metodo que retorna la fecha actual.
	 * @return fecha
	 */
	public static Timestamp getFecha(){
		Timestamp ts = Timestamp.from(Instant.now());
		return ts;
	}
  
}
