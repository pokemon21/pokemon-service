package mx.smart.rey.pokemon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.smart.rey.pokemon.bd.repository.IBitacoraRepository; 
import mx.smart.rey.pokemon.domain.Bitacora; 

@Service
public class InsertBitacoraService implements IInsertBitacoraService {
	
	@Autowired
	private IBitacoraRepository iUsuarioRepository;

	@Override
	public void insertBitacora(Bitacora bit) {
		iUsuarioRepository.save(bit); 
	}
}
