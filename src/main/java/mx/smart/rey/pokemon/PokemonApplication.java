package mx.smart.rey.pokemon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication; 
import org.springframework.cloud.openfeign.EnableFeignClients; 
import org.springframework.context.annotation.ComponentScan; 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */ 

@SpringBootApplication
@ComponentScan(basePackages = { "mx.smart.rey.pokemon" }) 
@EnableFeignClients
public class PokemonApplication {
	/**
	 * Metodo main para inicializar la aplicacion Spring Boot
	 * 
	 * @param args Argumentos opcionales de envio al programa
	 */
    public static void main(String[] args) {
		SpringApplication.run(PokemonApplication.class, args);
	} 
   
    
     
}