package mx.smart.rey.pokemon.repository;

import org.springframework.cloud.openfeign.FeignClient; 
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod; 
import mx.smart.rey.pokemon.model.response.DataResponse;
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */

@FeignClient( name = "IConsumeRepository", url = "${url.host}" )
public interface IConsumeRepository {

	@RequestMapping(value = "${url.path-host}", method = 
			RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, 
			consumes = MediaType.APPLICATION_JSON_VALUE ) 
	ResponseEntity<DataResponse> getByCompany(@PathVariable  String path);
}
