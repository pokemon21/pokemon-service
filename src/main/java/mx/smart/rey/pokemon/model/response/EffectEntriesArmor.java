package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;

/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */

public class EffectEntriesArmor implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String effect;
	private LanguageType language;
	private String short_effect;
	public String getEffect() {
		return effect;
	}
	public void setEffect(String effect) {
		this.effect = effect;
	}
	public LanguageType getLanguage() {
		return language;
	}
	public void setLanguage(LanguageType language) {
		this.language = language;
	}
	public String getShort_effect() {
		return short_effect;
	}
	public void setShort_effect(String short_effect) {
		this.short_effect = short_effect;
	}
	
	

}
