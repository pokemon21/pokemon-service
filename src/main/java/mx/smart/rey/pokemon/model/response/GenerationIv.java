package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class GenerationIv implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("diamond-pearl")
	private DiamondPearl diamond_pearl;
	@JsonProperty("heartgold-soulsilver")
	private HeartgoldSoulsilver heartgold_soulsilver;
	private Platinum platinum;
	
	
	
	public DiamondPearl getDiamond_pearl() {
		return diamond_pearl;
	}
	public void setDiamond_pearl(DiamondPearl diamond_pearl) {
		this.diamond_pearl = diamond_pearl;
	}
	public HeartgoldSoulsilver getHeartgold_soulsilver() {
		return heartgold_soulsilver;
	}
	public void setHeartgold_soulsilver(HeartgoldSoulsilver heartgold_soulsilver) {
		this.heartgold_soulsilver = heartgold_soulsilver;
	}
	public Platinum getPlatinum() {
		return platinum;
	}
	public void setPlatinum(Platinum platinum) {
		this.platinum = platinum;
	}
	
	
	

}
