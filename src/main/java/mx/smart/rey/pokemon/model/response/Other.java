package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class Other implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DreamWorld dream_world;
	private Home Home;
	private OfficialArtwork official_artwork;
	public DreamWorld getDream_world() {
		return dream_world;
	}
	public void setDream_world(DreamWorld dream_world) {
		this.dream_world = dream_world;
	}
	public Home getHome() {
		return Home;
	}
	public void setHome(Home home) {
		Home = home;
	}
	public OfficialArtwork getOfficial_artwork() {
		return official_artwork;
	}
	public void setOfficial_artwork(OfficialArtwork official_artwork) {
		this.official_artwork = official_artwork;
	}
	
	
	

}
