package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */

public class VersionDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 
	private String rarity; 
	private Version version;
	
	
	public String getRarity() {
		return rarity;
	}
	public void setRarity(String rarity) {
		this.rarity = rarity;
	}
	public Version getVersion() {
		return version;
	}
	public void setVersion(Version version) {
		this.version = version;
	}
	
	
	
	

}
