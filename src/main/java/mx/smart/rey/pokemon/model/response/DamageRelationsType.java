package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class DamageRelationsType implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private ArrayList<DoubleDamageFromType> double_damage_from;
	
	private ArrayList<DoubleDamageToType> double_damage_to;
	
	private ArrayList<DalfDamageFromType> half_damage_from;
	
	private ArrayList<HalfDamageToType> half_damage_to;
	
	private ArrayList<NoDamageFromType> no_damage_from;
	

	public ArrayList<DoubleDamageFromType> getDouble_damage_from() {
		return double_damage_from;
	}

	public void setDouble_damage_from(ArrayList<DoubleDamageFromType> double_damage_from) {
		this.double_damage_from = double_damage_from;
	}

	public ArrayList<DoubleDamageToType> getDouble_damage_to() {
		return double_damage_to;
	}

	public void setDouble_damage_to(ArrayList<DoubleDamageToType> double_damage_to) {
		this.double_damage_to = double_damage_to;
	}

	public ArrayList<DalfDamageFromType> getHalf_damage_from() {
		return half_damage_from;
	}

	public void setHalf_damage_from(ArrayList<DalfDamageFromType> half_damage_from) {
		this.half_damage_from = half_damage_from;
	}

	public ArrayList<HalfDamageToType> getHalf_damage_to() {
		return half_damage_to;
	}

	public void setHalf_damage_to(ArrayList<HalfDamageToType> half_damage_to) {
		this.half_damage_to = half_damage_to;
	}

	public ArrayList<NoDamageFromType> getNo_damage_from() {
		return no_damage_from;
	}

	public void setNo_damage_from(ArrayList<NoDamageFromType> no_damage_from) {
		this.no_damage_from = no_damage_from;
	}
	
	

}
