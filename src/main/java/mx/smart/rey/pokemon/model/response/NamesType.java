package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class NamesType implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private LanguageType language;
	private String name;

	public LanguageType getLanguage() {
		return language;
	}

	public void setLanguage(LanguageType language) {
		this.language = language;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
