package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
import java.util.ArrayList;
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class HeldItems implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 
	private Item item;
	 
	private ArrayList<VersionDetails> version_details;

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public ArrayList<VersionDetails> getVersion_details() {
		return version_details;
	}

	public void setVersion_details(ArrayList<VersionDetails> version_details) {
		this.version_details = version_details;
	}

	 
 
}
