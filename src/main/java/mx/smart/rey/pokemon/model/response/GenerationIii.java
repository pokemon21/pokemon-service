package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class GenerationIii implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Emerald emerald;
	@JsonProperty("firered-leafgreen")
	private FireredLeafgreen firered_leafgreen;
	@JsonProperty("ruby-sapphire")
	private RubySapphire ruby_sapphire;
	
	
	public Emerald getEmerald() {
		return emerald;
	}
	public void setEmerald(Emerald emerald) {
		this.emerald = emerald;
	}
	public FireredLeafgreen getFirered_leafgreen() {
		return firered_leafgreen;
	}
	public void setFirered_leafgreen(FireredLeafgreen firered_leafgreen) {
		this.firered_leafgreen = firered_leafgreen;
	}
	public RubySapphire getRuby_sapphire() {
		return ruby_sapphire;
	}
	public void setRuby_sapphire(RubySapphire ruby_sapphire) {
		this.ruby_sapphire = ruby_sapphire;
	}

}
