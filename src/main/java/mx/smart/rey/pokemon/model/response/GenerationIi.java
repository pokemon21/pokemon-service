package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class GenerationIi implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Crystal crystal;
	private Gold gold;
	private Silver silver;
	
	
	public Crystal getCrystal() {
		return crystal;
	}
	public void setCrystal(Crystal crystal) {
		this.crystal = crystal;
	}
	public Gold getGold() {
		return gold;
	}
	public void setGold(Gold gold) {
		this.gold = gold;
	}
	public Silver getSilver() {
		return silver;
	}
	public void setSilver(Silver silver) {
		this.silver = silver;
	}
	
	
	
	
	

}
