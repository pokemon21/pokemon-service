package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class FlavorTextEntriesAegislash implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String flavor_text;
	
	/*
	 * Type
	 */
	private LanguageType language;
	private Version version;
	
	
	public String getFlavor_text() {
		return flavor_text;
	}
	public void setFlavor_text(String flavor_text) {
		this.flavor_text = flavor_text;
	}
	public LanguageType getLanguage() {
		return language;
	}
	public void setLanguage(LanguageType language) {
		this.language = language;
	}
	public Version getVersion() {
		return version;
	}
	public void setVersion(Version version) {
		this.version = version;
	}
	
	
	

}
