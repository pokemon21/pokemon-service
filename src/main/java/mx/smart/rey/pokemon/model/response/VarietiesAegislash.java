package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class VarietiesAegislash implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String is_default;
	private PokemonType pokemon;
	public String getIs_default() {
		return is_default;
	}
	public void setIs_default(String is_default) {
		this.is_default = is_default;
	}
	public PokemonType getPokemon() {
		return pokemon;
	}
	public void setPokemon(PokemonType pokemon) {
		this.pokemon = pokemon;
	}
	
	

}
