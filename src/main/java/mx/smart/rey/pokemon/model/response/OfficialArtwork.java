package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class OfficialArtwork implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String front_default;

	public String getFront_default() {
		return front_default;
	}

	public void setFront_default(String front_default) {
		this.front_default = front_default;
	}
	
	

}
