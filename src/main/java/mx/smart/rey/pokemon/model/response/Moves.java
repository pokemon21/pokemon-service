package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
import java.util.ArrayList;
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class Moves implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L; 
	private Move move; 
	private ArrayList<VersionGroupDetails>  version_group_details;
	
	/*
	 * Type
	 */
	private String name;
	private String url;

	public Move getMove() {
		return move;
	}

	public void setMove(Move move) {
		this.move = move;
	}

	public ArrayList<VersionGroupDetails> getVersion_group_details() {
		return version_group_details;
	}

	public void setVersion_group_details(ArrayList<VersionGroupDetails> version_group_details) {
		this.version_group_details = version_group_details;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	 
}
