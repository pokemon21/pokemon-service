package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class GenerationViii implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Icons icons;

	public Icons getIcons() {
		return icons;
	}

	public void setIcons(Icons icons) {
		this.icons = icons;
	}
	
	

}
