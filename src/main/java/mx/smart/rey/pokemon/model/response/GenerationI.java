package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class GenerationI implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("red-blue")
	private RedBlue red_blue;
	private Yellow yellow;
	
	public RedBlue getRed_blue() {
		return red_blue;
	}
	public void setRed_blue(RedBlue red_blue) {
		this.red_blue = red_blue;
	}
	public Yellow getYellow() {
		return yellow;
	}
	public void setYellow(Yellow yellow) {
		this.yellow = yellow;
	}
	
	

}
