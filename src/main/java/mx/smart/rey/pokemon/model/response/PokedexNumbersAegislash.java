package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class PokedexNumbersAegislash implements Serializable { 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer entry_number;
	private PokedexAegislash pokedex;
	public Integer getEntry_number() {
		return entry_number;
	}
	public void setEntry_number(Integer entry_number) {
		this.entry_number = entry_number;
	}
	public PokedexAegislash getPokedex() {
		return pokedex;
	}
	public void setPokedex(PokedexAegislash pokedex) {
		this.pokedex = pokedex;
	}

}
