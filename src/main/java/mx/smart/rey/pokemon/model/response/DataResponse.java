package mx.smart.rey.pokemon.model.response;
 
import java.io.Serializable;
import java.util.ArrayList;
 
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType; 
import javax.xml.bind.annotation.XmlRootElement;
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="data")
public class DataResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 
	private ArrayList<Abilities> abilities; 
	private String base_experience; 
	private ArrayList<Forms> forms; 
	private ArrayList<GameIndices> game_indices; 
	private Integer height; 
	private ArrayList<HeldItems>  held_items; 
	private Integer id; 
	private Boolean is_default; 
	private String location_area_encounters;  
	private ArrayList<Moves> moves; 
	private String name;
	private Integer order;
	private ArrayList<String> past_types; 
	private Species species;
	private Sprites sprites;
	private ArrayList<Stats> stats;
	
	
	/*
	 * Type
	 */
	private DamageRelationsType damage_relations;
	private GenerationType generation;
	private ArrayList<NamesType> names;
	private ArrayList<PokemonListType> pokemon;
	/*
	 * pokemon-species/aegislash
	 */
	
	private String base_happiness;
	private String capture_rate;
	private ColorAegislash color;
	private ArrayList<eggGroupsAegislash> egg_groups;
	private EvolutionChainAegislash evolution_chain;
	private EvolvesFromSpeciesAegislash evolves_from_species;
	private ArrayList<FlavorTextEntriesAegislash> flavor_text_entries;
	private ArrayList<FormDescriptionsAegislash> form_descriptions;
	private Boolean forms_switchable;
	private Integer gender_rate;
	private ArrayList<GeneraAegislash> genera;
	private GrowthRateAegislash growth_rate;
	private String habitat;
	private Boolean has_gender_differences;
	private Integer hatch_counter;
	private Boolean is_baby;
	private Boolean is_legendary;
	private Boolean is_mythical; 
	private ArrayList<PokedexNumbersAegislash> pokedex_numbers;
	private ShapeAegislash shape;
	private ArrayList<VarietiesAegislash> varieties;
	
	private Boolean is_main_series;
	private ArrayList<EffectEntriesArmor> effect_entries;
	
	
	/*
	 * https://pokeapi.co/api/v2/pokemon?limit=100000&offset=0
	 */
	private Integer count;
	private String next;
	private String previous;
	private ArrayList<ResultsLimit> results;
	
	 

	public ArrayList<Abilities> getAbilities() {
		return abilities;
	}

	public void setAbilities(ArrayList<Abilities> abilities) {
		this.abilities = abilities;
	} 

	public ArrayList<Forms> getForms() {
		return forms;
	}

	public void setForms(ArrayList<Forms> forms) {
		this.forms = forms;
	}
 

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}
 

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
 

	public ArrayList<Moves> getMoves() {
		return moves;
	}

	public void setMoves(ArrayList<Moves> moves) {
		this.moves = moves;
	}

	public String getBase_experience() {
		return base_experience;
	}

	public void setBase_experience(String base_experience) {
		this.base_experience = base_experience;
	}

	public ArrayList<GameIndices> getGame_indices() {
		return game_indices;
	}

	public void setGame_indices(ArrayList<GameIndices> game_indices) {
		this.game_indices = game_indices;
	}

	public ArrayList<HeldItems> getHeld_items() {
		return held_items;
	}

	public void setHeld_items(ArrayList<HeldItems> held_items) {
		this.held_items = held_items;
	}

	public Boolean getIs_default() {
		return is_default;
	}

	public void setIs_default(Boolean is_default) {
		this.is_default = is_default;
	}

	public String getLocation_area_encounters() {
		return location_area_encounters;
	}

	public void setLocation_area_encounters(String location_area_encounters) {
		this.location_area_encounters = location_area_encounters;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public ArrayList<String> getPast_types() {
		return past_types;
	}

	public void setPast_types(ArrayList<String> past_types) {
		this.past_types = past_types;
	}

	public Species getSpecies() {
		return species;
	}

	public void setSpecies(Species species) {
		this.species = species;
	}

	public Sprites getSprites() {
		return sprites;
	}

	public void setSprites(Sprites sprites) {
		this.sprites = sprites;
	}

	public ArrayList<Stats> getStats() {
		return stats;
	}

	public void setStats(ArrayList<Stats> stats) {
		this.stats = stats;
	}

	/**
	 * Type
	 */
	public DamageRelationsType getDamage_relations() {
		return damage_relations;
	}

	public void setDamage_relations(DamageRelationsType damage_relations) {
		this.damage_relations = damage_relations;
	}

	public GenerationType getGeneration() {
		return generation;
	}

	public void setGeneration(GenerationType generation) {
		this.generation = generation;
	}

	public ArrayList<NamesType> getNames() {
		return names;
	}

	public void setNames(ArrayList<NamesType> names) {
		this.names = names;
	}

	public ArrayList<PokemonListType> getPokemon() {
		return pokemon;
	}

	public void setPokemon(ArrayList<PokemonListType> pokemon) {
		this.pokemon = pokemon;
	}

	public String getBase_happiness() {
		return base_happiness;
	}

	public void setBase_happiness(String base_happiness) {
		this.base_happiness = base_happiness;
	}

	public String getCapture_rate() {
		return capture_rate;
	}

	public void setCapture_rate(String capture_rate) {
		this.capture_rate = capture_rate;
	}

	public ColorAegislash getColor() {
		return color;
	}

	public void setColor(ColorAegislash color) {
		this.color = color;
	}

	public ArrayList<eggGroupsAegislash> getEgg_groups() {
		return egg_groups;
	}

	public void setEgg_groups(ArrayList<eggGroupsAegislash> egg_groups) {
		this.egg_groups = egg_groups;
	}

	public EvolutionChainAegislash getEvolution_chain() {
		return evolution_chain;
	}

	public void setEvolution_chain(EvolutionChainAegislash evolution_chain) {
		this.evolution_chain = evolution_chain;
	}

	public EvolvesFromSpeciesAegislash getEvolves_from_species() {
		return evolves_from_species;
	}

	public void setEvolves_from_species(EvolvesFromSpeciesAegislash evolves_from_species) {
		this.evolves_from_species = evolves_from_species;
	}

	public ArrayList<FlavorTextEntriesAegislash> getFlavor_text_entries() {
		return flavor_text_entries;
	}

	public void setFlavor_text_entries(ArrayList<FlavorTextEntriesAegislash> flavor_text_entries) {
		this.flavor_text_entries = flavor_text_entries;
	}

	public ArrayList<FormDescriptionsAegislash> getForm_descriptions() {
		return form_descriptions;
	}

	public void setForm_descriptions(ArrayList<FormDescriptionsAegislash> form_descriptions) {
		this.form_descriptions = form_descriptions;
	}

	public Boolean getForms_switchable() {
		return forms_switchable;
	}

	public void setForms_switchable(Boolean forms_switchable) {
		this.forms_switchable = forms_switchable;
	}

	public Integer getGender_rate() {
		return gender_rate;
	}

	public void setGender_rate(Integer gender_rate) {
		this.gender_rate = gender_rate;
	}

	public ArrayList<GeneraAegislash> getGenera() {
		return genera;
	}

	public void setGenera(ArrayList<GeneraAegislash> genera) {
		this.genera = genera;
	}

	public GrowthRateAegislash getGrowth_rate() {
		return growth_rate;
	}

	public void setGrowth_rate(GrowthRateAegislash growth_rate) {
		this.growth_rate = growth_rate;
	}

	public String getHabitat() {
		return habitat;
	}

	public void setHabitat(String habitat) {
		this.habitat = habitat;
	}

	public Boolean getHas_gender_differences() {
		return has_gender_differences;
	}

	public void setHas_gender_differences(Boolean has_gender_differences) {
		this.has_gender_differences = has_gender_differences;
	}

	public Integer getHatch_counter() {
		return hatch_counter;
	}

	public void setHatch_counter(Integer hatch_counter) {
		this.hatch_counter = hatch_counter;
	}

	public Boolean getIs_baby() {
		return is_baby;
	}

	public void setIs_baby(Boolean is_baby) {
		this.is_baby = is_baby;
	}

	public Boolean getIs_legendary() {
		return is_legendary;
	}

	public void setIs_legendary(Boolean is_legendary) {
		this.is_legendary = is_legendary;
	}

	public Boolean getIs_mythical() {
		return is_mythical;
	}

	public void setIs_mythical(Boolean is_mythical) {
		this.is_mythical = is_mythical;
	}

	public ArrayList<PokedexNumbersAegislash> getPokedex_numbers() {
		return pokedex_numbers;
	}

	public void setPokedex_numbers(ArrayList<PokedexNumbersAegislash> pokedex_numbers) {
		this.pokedex_numbers = pokedex_numbers;
	}

	public ShapeAegislash getShape() {
		return shape;
	}

	public void setShape(ShapeAegislash shape) {
		this.shape = shape;
	}

	public ArrayList<VarietiesAegislash> getVarieties() {
		return varieties;
	}

	public void setVarieties(ArrayList<VarietiesAegislash> varieties) {
		this.varieties = varieties;
	}

	public Boolean getIs_main_series() {
		return is_main_series;
	}

	public void setIs_main_series(Boolean is_main_series) {
		this.is_main_series = is_main_series;
	}

	public ArrayList<EffectEntriesArmor> getEffect_entries() {
		return effect_entries;
	}

	public void setEffect_entries(ArrayList<EffectEntriesArmor> effect_entries) {
		this.effect_entries = effect_entries;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public String getPrevious() {
		return previous;
	}

	public void setPrevious(String previous) {
		this.previous = previous;
	}

	public ArrayList<ResultsLimit> getResults() {
		return results;
	}

	public void setResults(ArrayList<ResultsLimit> results) {
		this.results = results;
	}
 
	
	
	
	
	
 
}
