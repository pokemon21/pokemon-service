package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class VersionGroupDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("level_learned_at")
	private Integer level_learned_at;
	
	@JsonProperty("move_learn_method")
	private MoveIearnMethod move_learn_method;
	

	@JsonProperty("version_group")
	private VersionGroup group;
	
	
 

	public VersionGroup getGroup() {
		return group;
	}

	public void setGroup(VersionGroup group) {
		this.group = group;
	}

	public Integer getLevel_learned_at() {
		return level_learned_at;
	}

	public void setLevel_learned_at(Integer level_learned_at) {
		this.level_learned_at = level_learned_at;
	}

	public MoveIearnMethod getMove_learn_method() {
		return move_learn_method;
	}

	public void setMove_learn_method(MoveIearnMethod move_learn_method) {
		this.move_learn_method = move_learn_method;
	}
	
	
	
	

}
