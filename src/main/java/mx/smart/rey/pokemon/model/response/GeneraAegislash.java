package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class GeneraAegislash implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String genus;
	private LanguageType language;
	public String getGenus() {
		return genus;
	}
	public void setGenus(String genus) {
		this.genus = genus;
	}
	public LanguageType getLanguage() {
		return language;
	}
	public void setLanguage(LanguageType language) {
		this.language = language;
	}
	
	
	

}
