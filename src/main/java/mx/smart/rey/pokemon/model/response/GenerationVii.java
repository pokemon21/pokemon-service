package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class GenerationVii implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Icons icons;
	
	@JsonProperty("ultra-sun-ultra-moon")
	private UltraSunUltraMoon ultra_sun_ultra_moon;
	
	
	public Icons getIcons() {
		return icons;
	}
	public void setIcons(Icons icons) {
		this.icons = icons;
	}
	public UltraSunUltraMoon getUltra_sun_ultra_moon() {
		return ultra_sun_ultra_moon;
	}
	public void setUltra_sun_ultra_moon(UltraSunUltraMoon ultra_sun_ultra_moon) {
		this.ultra_sun_ultra_moon = ultra_sun_ultra_moon;
	}
	
	
	
	
	

}
