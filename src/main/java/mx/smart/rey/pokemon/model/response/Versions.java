package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class Versions implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("generation-i")
	private GenerationI generation_i;
	

	@JsonProperty("generation-ii")
	private GenerationIi generation_ii;

	@JsonProperty("generation-iii")
	private GenerationIii generation_iii;
	

	@JsonProperty("generation-iv")
	private GenerationIv generation_iv;
	
	@JsonProperty("generation-v")
	private GenerationV generation_v;
	
	@JsonProperty("generation-vi")
	private Generationvi generation_vi;
	
	
	
	
	
	@JsonProperty("generation-vii")
	private GenerationVii generation_vii;
	
	@JsonProperty("generation-viii")
	private GenerationViii generation_viii;
	
	
	
	

	public GenerationI getGeneration_i() {
		return generation_i;
	}

	public void setGeneration_i(GenerationI generation_i) {
		this.generation_i = generation_i;
	}

	public GenerationIi getGeneration_ii() {
		return generation_ii;
	}

	public void setGeneration_ii(GenerationIi generation_ii) {
		this.generation_ii = generation_ii;
	}

	public GenerationIii getGeneration_iii() {
		return generation_iii;
	}

	public void setGeneration_iii(GenerationIii generation_iii) {
		this.generation_iii = generation_iii;
	}

	public GenerationIv getGeneration_iv() {
		return generation_iv;
	}

	public void setGeneration_iv(GenerationIv generation_iv) {
		this.generation_iv = generation_iv;
	}

	public GenerationV getGeneration_v() {
		return generation_v;
	}

	public void setGeneration_v(GenerationV generation_v) {
		this.generation_v = generation_v;
	}

	public Generationvi getGeneration_vi() {
		return generation_vi;
	}

	public void setGeneration_vi(Generationvi generation_vi) {
		this.generation_vi = generation_vi;
	}

	public GenerationVii getGeneration_vii() {
		return generation_vii;
	}

	public void setGeneration_vii(GenerationVii generation_vii) {
		this.generation_vii = generation_vii;
	}

	public GenerationViii getGeneration_viii() {
		return generation_viii;
	}

	public void setGeneration_viii(GenerationViii generation_viii) {
		this.generation_viii = generation_viii;
	}
	
	
	

}
