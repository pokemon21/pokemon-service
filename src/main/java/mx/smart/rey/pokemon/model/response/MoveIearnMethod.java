package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class MoveIearnMethod implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("url")
	private String url;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	
	

}
