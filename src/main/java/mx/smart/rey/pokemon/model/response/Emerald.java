package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class Emerald implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String front_default;
	private String front_shiny;
	public String getFront_default() {
		return front_default;
	}
	public void setFront_default(String front_default) {
		this.front_default = front_default;
	}
	public String getFront_shiny() {
		return front_shiny;
	}
	public void setFront_shiny(String front_shiny) {
		this.front_shiny = front_shiny;
	}
	
	
	

}
