package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class Generationvi implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("omegaruby-alphasapphire")
	private OmegarubyAlphasapphire omegaruby_alphasapphire;
	@JsonProperty("x-y")
	private Xy x_y;
	public OmegarubyAlphasapphire getOmegaruby_alphasapphire() {
		return omegaruby_alphasapphire;
	}
	public void setOmegaruby_alphasapphire(OmegarubyAlphasapphire omegaruby_alphasapphire) {
		this.omegaruby_alphasapphire = omegaruby_alphasapphire;
	}
	public Xy getX_y() {
		return x_y;
	}
	public void setX_y(Xy x_y) {
		this.x_y = x_y;
	}
}
