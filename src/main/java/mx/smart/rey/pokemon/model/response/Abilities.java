package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class Abilities implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
 
	private Ability ability;
	 
	private String is_hidden;
	 
	private Integer slot;
	
	public Ability getAbility() {
		return ability;
	}
	public void setAbility(Ability ability) {
		this.ability = ability;
	}
	 
	public Integer getSlot() {
		return slot;
	}
	public void setSlot(Integer slot) {
		this.slot = slot;
	}
	public String getIs_hidden() {
		return is_hidden;
	}
	public void setIs_hidden(String is_hidden) {
		this.is_hidden = is_hidden;
	} 
	
	
	

}
