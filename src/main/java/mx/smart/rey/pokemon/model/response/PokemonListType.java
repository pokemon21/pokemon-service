package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class PokemonListType implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PokemonType pokemon;
	private String slot;
	public PokemonType getPokemon() {
		return pokemon;
	}
	public void setPokemon(PokemonType pokemon) {
		this.pokemon = pokemon;
	}
	public String getSlot() {
		return slot;
	}
	public void setSlot(String slot) {
		this.slot = slot;
	}
	
	

}
