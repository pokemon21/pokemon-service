package mx.smart.rey.pokemon.model.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
public class GenerationV implements Serializable {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("black-white")
	private BlackWhite black_white;

	
	public BlackWhite getBlack_white() {
		return black_white;
	}
	public void setBlack_white(BlackWhite black_white) {
		this.black_white = black_white;
	}
	
	
	
}
