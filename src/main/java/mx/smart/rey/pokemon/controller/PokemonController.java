package mx.smart.rey.pokemon.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.smart.rey.pokemon.domain.Bitacora;
import mx.smart.rey.pokemon.model.response.DataResponse;
import mx.smart.rey.pokemon.repository.IConsumeRepository;
import mx.smart.rey.pokemon.service.IInsertBitacoraService;
import mx.smart.rey.pokemon.util.Utils; 
 
/**
 * Smart Rey 
 * @author Reynaldo Ivan Martinez Lopez.
 *
 */
   
@RestController
@RequestMapping("/") 
public class PokemonController {

	/** La Constante LOGGER. Obtiene el Logger de la clase */
	private static final Logger LOGGER = LoggerFactory.getLogger(PokemonController.class);

	/**
	 * Acceso a las operaraciones del service de inversion.
	 */ 
	
	@Autowired
	private IConsumeRepository consumeRepository;
	
	@Autowired
	private IInsertBitacoraService iInsertBitacoraService;
	 
  
	/**
	 * Despliega la informacion del pokemon
	 *  
	 * @return Objeto con la respuesta del servicio
	 */ 
	@GetMapping(value = "/pokemon",produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<DataResponse> pokemon(@RequestParam String path,
			@RequestHeader String ip) {
		LOGGER.info("-----------------------------INICIO DEL SERVICIO POKEMON------ "); 
		LOGGER.info("--------path------ {}",path);
		ResponseEntity<DataResponse> response = consumeRepository.getByCompany(path);
		LOGGER.info("--------IP------ {}",ip);
		Bitacora bitacora = new Bitacora();
		bitacora.setFecha(Utils.getFecha());
		bitacora.setIpOrigen(ip);
		bitacora.setMetodoEjec(path); 
		iInsertBitacoraService.insertBitacora(bitacora);
		return new ResponseEntity<DataResponse>( response.getBody(), response.getStatusCode() );
	}
}