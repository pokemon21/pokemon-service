package mx.smart.rey.pokemon.domain;

import java.io.Serializable;
import java.sql.Timestamp; 

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bitacora")
public class Bitacora implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7010308376508766406L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID_BITACORA",unique=true, nullable = false)
	private Integer idBit;
	
	@Column(name = "IP_ORIGEN")
	private String ipOrigen;
	
	@Column(name = "FECHA")
	private Timestamp fecha;
	
	@Column(name = "METODO_EJEC")
    private String metodoEjec;

	
	
	public Integer getIdBit() {
		return idBit;
	}

	public void setIdBit(Integer idBit) {
		this.idBit = idBit;
	}

	public String getIpOrigen() {
		return ipOrigen;
	}

	public void setIpOrigen(String ipOrigen) {
		this.ipOrigen = ipOrigen;
	}
 

	public String getMetodoEjec() {
		return metodoEjec;
	}

	public void setMetodoEjec(String metodoEjec) {
		this.metodoEjec = metodoEjec;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	} 
	
}
  
